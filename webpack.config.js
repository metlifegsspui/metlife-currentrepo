var path = require('path');

module.exports = {
	entry: path.resolve(__dirname, 'jsx/mainPage.js'),
	// Set this if you want webpack-dev-server to delegate a single path to an arbitrary server.
	// Use "*" to proxy all paths to the specified server.
	// This is useful if you want to get rid of 'http://localhost:8080/' in script[src],
	// and has many other use cases (see https://github.com/webpack/webpack-dev-server/pull/127 ).
	proxy: {
		"*": "http://172.26.60.5:7776"
	},
	publicPath: "/",
	host: "0.0.0.0",
	module: {
		loaders: [
			{
				test: /\.js$/,
				loader: 'babel',
				query: {stage: 0, plugins: ['./babelRelayPlugin']}
			}
		]
	},
	output: {filename: 'generated/myAccounts.bundle.js', path: './'}
}
