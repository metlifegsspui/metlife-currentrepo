
let React = require('react');
let ReactDOM = require('react-dom');
let Relay = require('react-relay');

//let UserPolicies = require('./userPoliciesBoxes');
//let Policy = require('./policyBox');
let Policy = require('./policyBox');

//import React, { PropTypes, Component } from 'react';


import Header from '../src/components/Header';
import Footer from '../src/components/Footer';


module.exports = Relay.createContainer(module.exports = class UserPolicies extends React.Component {
/*
		static propTypes = {
			children: PropTypes.element.isRequired,
			error: PropTypes.object
		};
*/
		render() {

			let policies = this.props.userpolicies.policies.edges.map(
				(policy, i) => <Policy policy={policy.node} key={i} />
			);

			return <ul className="noListStyle">
				<Header />
				{ policies }
				<li className="whiteBox col-xs-4">
					<img src="../images/boxEDelivery.png" className="autoBox"/>
				</li>
				<li className="whiteBox col-xs-4">
					<img src="../images/boxAuto.png" className="autoBox"/>
				</li>
				<li className="whiteBox col-xs-4">
					<img src="../images/boxHome.png" className="autoBox"/>
				</li>

			</ul>

			/*
			return !this.props.error ? (
				<div>
					<Header />
					{this.props.children}
					<Footer />
				</div>
			) : this.props.children;
			*/
		}
	}
	, {
    fragments: {
        userpolicies: ((Component) => {
            return Relay.QL`
			fragment on User {
				policies(first: 10) {
					edges {
						node {
							${Policy.getFragment('policy')}
						}
					}
				}
			}
			`
        })
    }
});
