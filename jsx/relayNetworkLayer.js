
let React = require('react');
let ReactDOM = require('react-dom');
let Relay = require('react-relay');

module.exports = Relay.injectNetworkLayer(
    //new Relay.DefaultNetworkLayer('http://localhost:8793/graphql')
    new Relay.DefaultNetworkLayer('http://localhost:3000/graphql')
);
