
let React = require('react');
let ReactDOM = require('react-dom');
let Relay = require('react-relay');
//let Header = require('../src/components/Header');
//import Header from '../src/components/Header';
import Router from 'react-routing/src/Router';


let GSSPRoute = require('./queryBase');
let relayNetworkLayer = require('./relayNetworkLayer');
//let UserPolicies = require('./userPoliciesBoxes');

let UserPolicies = require('./userPolicies');
//import App from '../src/components/App';

//return component && <App context={state.context}>{component}</App>;
//import App from '../src/components/App';

const router = new Router(on => {
    on('*', async (state, next) => {
        const component = await next();
        return component && <App context={state.context}>{component}</App>;
    });
});

let mountNode = document.getElementById('attachReactHere');
let rootComponent = <Relay.RootContainer Component={UserPolicies} route={new GSSPRoute({userID: window.btoa("6")})} />;
ReactDOM.render(rootComponent, mountNode);
