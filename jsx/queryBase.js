
let React = require('react');
let ReactDOM = require('react-dom');
let Relay = require('react-relay');

module.exports = class GSSPRoute extends Relay.Route {
    static routeName = 'GSSPRoute';
    //dXNlcjpudWxs

    static queries = {
        userpolicies: ((Component) => {
            return Relay.QL`
			query root {
					node(id: $userID) {
					${Component.getFragment('userpolicies')}
				}
			}
			`
        })
    };

}
