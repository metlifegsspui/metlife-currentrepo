
let React = require('react');
let ReactDOM = require('react-dom');
let Relay = require('react-relay');

//let Policy = require('./policyBox');

module.exports = Relay.createContainer(
	React.createClass ({
		render() {
			let policy = this.props.policy;

			let todayDate = new Date().toJSON().slice(0,10);

			//format camelcase
			let formattedAccountLoB = policy.accountLoB.replace(/([A-Z])/g, ' $1').replace(/^./, function(str){ return str.toUpperCase(); });

			return (
				<span>

            <li className="col-xs-4 whiteBox noPadding">
				<section className="titleSection">
					{policy.accountType == 'Life' ? <span>
											<img src="../images/blueLifeIcon.png" className=" top5Margin floatLeft"/>
					</span> : <span></span>
					}
					{policy.accountType == 'Dental' ? <span>
											<img src="../images/blueDentalIcon.png" className=" top5Margin floatLeft"/>
					</span> : <span></span>
					}
					{policy.accountType == 'Annuity' ? <span>
											<img src="../images/blueAnnuityIcon.png" className=" top5Margin floatLeft"/>
					</span> : <span></span>
					}
					<h4 className="title floatLeft topBottomPadding left5Margin">{policy.accountType}</h4>
					<img src="../images/bluePhone.png" className=" top5Margin floatRight"/>

				</section>
				<section className="subTitleSection">
					{policy.accountType == 'Life' ? <span>
											<section className="floatRight">
												<h4 className="rightTitle">${policy.faceAmt}</h4>
												<label className="rightSubTitle">Face Amount As Of: </label>
												<label className="rightSubTitle block">{todayDate}</label>
											</section>
					</span> : <span></span>
					}
					<h4 className="title">{formattedAccountLoB}</h4>
					<section className="font12"><label>Policy No.</label>{policy.accountNumber}</section>
					<div className="">

						{policy.RefDataItems.map(function(RefDataItem, i) {
							return (
								<select className="selectMenu" key={RefDataItem}>
									{RefDataItem.RefId.map(function(RefId, i) {
										return (
											<option key={RefId.RefId}>
												{RefId.RefId}
											</option>
										);
									})}
									{RefDataItem.RefDataItem.map(function(RefDataItems, i) {
										return (
											<option key={RefDataItems.loookupid}>
												{RefDataItems.lookupvalue}
											</option>
										);
									})}
								</select>
							);
						})}
					</div>

				</section>
				<section className="bodySection">
					{policy.accountType == 'Life'? <span>
                    <img src="../images/makePaymentBtn.png" className="floatRight"/>
                        <section className="payment"> <label>Payment Due:&nbsp;</label>${policy.billingInfo.paymentDueAmt}</section>
                        <section><label>Billing Options:&nbsp;</label>{policy.billingInfo.billingOption}</section>
                        <div className="subtitle">Financial Information &amp; Allocations</div></span>
						:<span></span> }
					{policy.accountType == 'Dental'? <span>
                    <img src="../images/makePaymentBtn.png" className="floatRight"/>
                        <section className="payment"> <label>Payment Due:&nbsp;</label>${policy.billingInfo.paymentDueAmt}</section>
                        <section><label>Billing Options:&nbsp;</label>{policy.billingInfo.billingOption}</section>
                        <div className="subtitle">Financial Information &amp; Allocations</div></span>
						:<span></span> }
					{policy.accountType == 'Life'? <span>
					<section><label>Face Amount:&nbsp;</label>${policy.faceAmt}</section>
					<section><label>Death Benefit:&nbsp;</label>${policy.deathBenefit}</section> </span>
						:<span></span> }
					{policy.accountType == 'Dental'? <span>
					<section><label>Deductible:&nbsp;</label>${policy.deductible}</section>
					<section><label>Max Yearly Benefit:&nbsp;</label>${policy.maxYearlyBenefit}</section> </span>
						:<span></span> }
					{policy.accountType == 'Annuity'? <span>
					<section><label>Monthly Benefit:&nbsp;</label>${policy.monthlyBenefit}</section>
					<section><label>Lump Sum Benefit as of {todayDate}:&nbsp;</label>${policy.lumpSumBenefit}</section> </span>
						:<span></span> }
					<div className="subtitle">General Information</div>
					{policy.accountType == 'Annuity'? <span>
					<section><label>Owner: &nbsp;</label>{policy.memberInfo.firstName}&nbsp;{policy.memberInfo.lastName}</section></span>
						:<span><section><label>Insured:&nbsp;</label>{policy.memberInfo.firstName}&nbsp;{policy.memberInfo.lastName}</section>
                    </span>}

					<section><label>Policy Status:&nbsp;</label>{policy.status}</section>
					{policy.accountType == 'Life' ? <span>
					<section><label>Beneficiary:&nbsp;</label>
						<li>{ policy.beneficiaries.map(function (beneficiary, key) {
							return <section key={beneficiary.beneficiaryId}>
								<li>{beneficiary.type == 'Primary' ?
									<span>{beneficiary.firstName}&nbsp;{beneficiary.lastName}&nbsp;
										({beneficiary.type})</span>
									:
									<span></span>
								}
								</li>
							</section>;
						}) }</li>
						<li>{ policy.beneficiaries.map(function (beneficiary, key) {
							return <section key={beneficiary.beneficiaryId}>
								<li>{beneficiary.type == 'Secondary' ?
									<span>{beneficiary.firstName}&nbsp;{beneficiary.lastName}&nbsp;
										({beneficiary.type})</span>
									:
									<span></span>
								}
								</li>
							</section>;
						}) }</li>
					</section>
						</span>
						: <span></span>
					}
					{policy.accountType == 'Annuity' ? <span>
					<section><label>Beneficiary:&nbsp;</label>
						<li>{ policy.beneficiaries.map(function (beneficiary, key) {
							return <section key={beneficiary.beneficiaryId}>
								<li>{beneficiary.type == 'Primary' ?
									<span>{beneficiary.firstName}&nbsp;{beneficiary.lastName}&nbsp;
										({beneficiary.type})</span>
									:
									<span></span>
								}
								</li>
							</section>;
						}) }</li>
						<li>{ policy.beneficiaries.map(function (beneficiary, key) {
							return <section key={beneficiary.beneficiaryId}>
								<li>{beneficiary.type == 'Secondary' ?
									<span>{beneficiary.firstName}&nbsp;{beneficiary.lastName}&nbsp;
										({beneficiary.type})</span>
									:
									<span></span>
								}
								</li>
							</section>;
						}) }</li>
					</section>
						</span>
						: <span></span>
					}
					<img src="../images/additionalDetailsBtn.png" className="floatRight"/>
					<div className="updateTxt">UPDATE</div>
				</section>
			</li>
                </span>
			)

		}
	})

	, {
    fragments: {
        policy: () => Relay.QL`
			fragment on Policy {
				memberInfo{
					firstName,
					lastName,
					dateOfBirth
				}
				billingInfo{
					paymentDueAmt,
					billingOption
				}
				beneficiaries {
					firstName,
					lastName,
					type,
					beneficiaryId
				}
				deathBenefit,
				status,
				accountNumber,
				faceAmt,
				accountType,
				accountLoB,
				maxYearlyBenefit,
				deductible,
				lumpSumBenefit,
				monthlyBenefit,
				RefDataItems {
					RefDataItem{
						RefId,
						lookupid,
						lookupvalue
					}
					RefId{
						RefId,
					}
				}
			}
		`
    }
});
