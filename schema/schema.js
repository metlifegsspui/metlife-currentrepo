/**
 *  Copyright (c) 2015, Facebook, Inc.
 *  All rights reserved.
 *
 *  This source code is licensed under the BSD-style license found in the
 *  LICENSE file in the root directory of this source tree. An additional grant
 *  of patent rights can be found in the PATENTS file in the same directory.
 */

import {
  GraphQLBoolean,
  GraphQLFloat,
  GraphQLID,
  GraphQLInt,
  GraphQLList,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
  GraphQLUnionType
} from 'graphql';

import {
  connectionArgs,
  connectionDefinitions,
  connectionFromArray,
  fromGlobalId,
  globalIdField,
  mutationWithClientMutationId,
  nodeDefinitions,
} from 'graphql-relay';
/*
import {
  // Import methods that your schema can use to interact with your database
  User,
  Widget,
  //Customer,
  getUser,
  getViewer,
  getWidget,
  getWidgets,
  getCustomer,
  getAddresses,
  getAccounts,
  Address,
  Account,
} from './database';
*/
/*
import Customer from './user';
*/
/**
 * We get the node interface and field from the Relay library.
 *
 * The first method defines the way we resolve an ID to its object.
 * The second defines the way we resolve an object to its GraphQL type.
 */
var {nodeInterface, nodeField} = nodeDefinitions(
  (globalId) => {
    var {type, id} = fromGlobalId(globalId);
    if (type === 'Policy') {
      //return getPolicy(id);
      return 123456;
    }
    else if (type === 'HealthPolicy'){
        return 45678;
      }
    /*
    } else if (type === 'Widget') {
      return getWidget(id);
    */
     else {
      return null;
    }
  },
  (obj) => {
    if (obj instanceof Policy) {
      return policyType;
    }
     else if (obj instanceof HealthPolicy){
      return healthPolicyType;
    }
    else if (obj instanceof Search){
      return policyUnionType;
    }/*else if (obj instanceof Widget)  {
      return widgetType;
    }*//*  else if (obj instanceof Customer)  {
      return customerType;
    }
	else if (obj instanceof Address)  {
      return addressType;
    }else if (obj instanceof Account)  {
      return accountType;
    }	else {
      return null;
    }
    */
  }
);

/**
 * Define your own types here
 */
    /*
var innerPolicyType = new GraphQLObjectType({
  name: 'policies',
  description: "policies",

  fields: () => ({
    id: globalIdField('Policy'),
    Insured: {
      type: GraphQLString,
      description: 'The name of the insured',
    },
    Status: {
      type: GraphQLString,
      description: 'The status of the policy',
    },
    Beneficiaries: {
      type: GraphQLString,
      description: 'The beneficiaries of the policy',
    },
    BillingOptions: {
      type: GraphQLString,
      description: 'The billing options for the policy',
    },
    FaceAmount: {
      type: GraphQLString,
      description: 'The face amount of the policy',
    },
    PolicyNum: {
      type: GraphQLString,
      description: 'The policyNum',
    },
    PaymentDue: {
      type: GraphQLString,
      description: 'The date the payment is due',
    },
    FaceAmount: {
      type: GraphQLString,
      description: 'The face amount of the policy',
    },
    DeathBenefit: {
      type: GraphQLString,
      description: 'The death benefit of the policy',
    },
    ProductName: {
      type: GraphQLString,
      description: 'The name of the product',
    },
    PolicyDescription: {
      type: GraphQLString,
      description: 'The description of the policy',
    }

  }),
  interfaces: [nodeInterface],
});
*/

var policyType = new GraphQLObjectType({
  name: 'Policy',
  description: 'A particular policy',
  args: connectionArgs,

  fields: () => ({
      id: globalIdField('Policy'),
      Insured: {
        type: GraphQLString,
        description: 'The name of the insured',
      },
      Status: {
        type: GraphQLString,
        description: 'The status of the policy',
      },
      Beneficiaries: {
        type: GraphQLString,
        description: 'The beneficiaries of the policy',
      },
      BillingOptions: {
        type: GraphQLString,
        description: 'The billing options for the policy',
      },
      FaceAmount: {
        type: GraphQLString,
        description: 'The face amount of the policy',
      },
      PolicyNum: {
        type: GraphQLString,
        description: 'The policyNum',
      },
      PaymentDue: {
        type: GraphQLString,
        description: 'The date the payment is due',
      },
      FaceAmount: {
        type: GraphQLString,
        description: 'The face amount of the policy',
      },
      DeathBenefit: {
        type: GraphQLString,
        description: 'The death benefit of the policy',
      },
      ProductName: {
        type: GraphQLString,
        description: 'The name of the product',
      },
      PolicyDescription: {
        type: GraphQLString,
        description: 'The description of the policy',
      }

    }),
    interfaces: [nodeInterface],
    /*
    policies: {
      type: innerPolicyType,

    }
    */
	/*customer: {
    args: {
          id: {
            name: 'id',
            type: new GraphQLNonNull(GraphQLString)
          }
        },
		type: customerType,
		resolve: (root, {id}, source, fieldASTs) => Customer.findById(id),
	},*/

	/*,
    widgets: {
      type: widgetConnection,
      description: 'A person\'s collection of widgets',
      args: connectionArgs,
      resolve: (_, args) => connectionFromArray(getWidgets(), args),
    },*/

});


var healthPolicyType = new GraphQLObjectType({
  name: 'HealthPolicy',
  description: 'A health policy',

  fields: () => ({
    id: globalIdField('Policy'),
    Insured : {
      type: GraphQLString,
      description: 'The name of the insured',
    },
    Status : {
      type: GraphQLString,
      description: 'The status of the policy',
    },
    Deductible : {
      type: GraphQLString,
      description: 'The policy deductible',
    },
    ProductName : {
      type: GraphQLString,
      description: 'The name of the product',
    },
    PolicyDescription : {
      type: GraphQLString,
      description: 'The description of the policy',
    }
  }),
  interfaces: [nodeInterface],
});
/*
var policyUnionType = new GraphQLUnionType({
  name: "UnionPolicy",
  description: "Union policy",
  types: [policyType,healthPolicyType],
  fields: () => ({
    id: globalIdField('Policy')
  }),
  resolveType(root){
    return policyType;
  },
  interfaces: [nodeInterface],
});
*/
/*
var widgetType = new GraphQLObjectType({
  name: 'Widget',
  description: 'A shiny widget',
  fields: () => ({
    id: globalIdField('Widget'),
    name: {
      type: GraphQLString,
      description: 'The name of the widget',
    },
  }),
  interfaces: [nodeInterface],
});
*/
/* OLD
var customerType = new GraphQLObjectType({
  name: 'Customer',
  description: 'A Customer',
  fields: () => ({
    id: globalIdField('Customer'),
    name: {
      type: GraphQLString,
      description: 'The name of the customer',
    },
	addresses:{
		type: addressConnection,
		
		args: connectionArgs,
      resolve: (_, args) => connectionFromArray(getAddresses(), args),
	},
	accounts:{
		type: accountConnection,
		
		args: connectionArgs,
      resolve: (_, args) => connectionFromArray(getAccounts(), args),
	},
  }),
});

var addressType = new GraphQLObjectType({
  name: 'Address',
  description: 'A Address',
  fields: () => ({
    
    addType: {
      type: GraphQLString,
      description: 'The type of the address',
    },
	addressLine1: {
      type: GraphQLString,
      description: 'The type of the address',
    },
	addressLine2: {
      type: GraphQLString,
      description: 'The type of the address',
    },
	city: {
      type: GraphQLString,
      description: 'The type of the city',
    },
	state: {
      type: GraphQLString,
      description: 'The type of the state',
    },
  }),
});

var accountType = new GraphQLObjectType({
  name: 'Account',
  description: 'A Account',
  fields: () => ({
    
    accType: {
      type: GraphQLString,
      description: 'The type of the address',
    },
	accNumber: {
      type: GraphQLString,
      description: 'The type of the address',
    },
	balance: {
      type: GraphQLString,
      description: 'The type of the address',
    },
	openDate: {
      type: GraphQLString,
      description: 'The type of the city',
    },
	
  }),
});
*/
/*
var addressType = new GraphQLObjectType({
  name : 'Address',
  description : 'Address .......',
  fields: () => ({
    addressType: { type: GraphQLString },
    streetNumber: { type: GraphQLString },
    streetName: { type: GraphQLString },
    city: { type: GraphQLString },             
    state: { type: GraphQLString },
    zipcode: { type: GraphQLString },    
    country: {  type: GraphQLString } 
  })
});

var accountType = new GraphQLObjectType({
  name : 'Account',
  description : 'Account ....',
  fields: () => ({
    accountType : {
      type : GraphQLString
    },
    accountNumber : {
      type : GraphQLString
    },
    balance : {
      type : GraphQLFloat
    },
    startDate : {
      type : GraphQLString
    }      
  })
});
*/
/*
var customerType = new GraphQLObjectType({
  name: 'Customer',
  description: 'Customer creator',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLString),
      description: 'The id of the customer.',
    },
    firstName: {
      type: GraphQLString,
      description: 'The first name of the user.',
    },
    lastName: {
      type: GraphQLString,
      description: 'The last name of the user.',
    },
    contactNumber: {
      type: GraphQLString,
      description: 'The contact number of the user.',
    }*//*,
    accounts: {
      type: new GraphQLList(accountType),
      description: '.......... or an empty list if they have none.'
    },
    addresses : {
      type : new GraphQLList(addressType),
      description : '..........'
    }*/
/*
  })
});
*/
/**
 * Define your own connection types here
 */
    /*
var {connectionType: widgetConnection} =
  connectionDefinitions({name: 'Widget', nodeType: widgetType});
  */
/*
  var {connectionType: accountConnection} =
  connectionDefinitions({name: 'Account', nodeType: accountType});

  var {connectionType: addressConnection} =
  connectionDefinitions({name: 'Address', nodeType: addressType});
*/
/**
 * This is the type that will be the root of our query,
 * and the entry point into our schema.
 */
var queryType = new GraphQLObjectType({
  name: 'policy',
  fields: () => ({
    //node: nodeField,
    // Add your own root fields here

    viewer: {
      type: policyType,
      resolve: () => getViewer(),
    },

    viewer2:{
      type: healthPolicyType,
      resolve: () => getViewer(),
    },
    /*
    search: {
      type: policyUnionType,
      resolve: () => getViewer(),
    },
    */
      id: globalIdField('Policy'),
      /*customer: {
       args: {
       id: {
       name: 'id',
       type: new GraphQLNonNull(GraphQLString)
       }
       },
       type: customerType,
       resolve: (root, {id}, source, fieldASTs) => Customer.findById(id),
       },*/
    /*
      Insured : {
        type: GraphQLString,
        description: 'The name of the insured',
      },
      Status : {
        type: GraphQLString,
        description: 'The status of the policy',
      },
      Beneficiaries : {
        type: GraphQLString,
        description: 'The beneficiaries of the policy',
      },
      BillingOptions : {
        type: GraphQLString,
        description: 'The billing options for the policy',
      },
      FaceAmount : {
        type: GraphQLString,
        description: 'The face amount of the policy',
      },
      PolicyNum : {
        type: GraphQLString,
        description: 'The policyNum',
      },
      PaymentDue : {
        type: GraphQLString,
        description: 'The date the payment is due',
      },
      FaceAmount : {
        type: GraphQLString,
        description: 'The face amount of the policy',
      },
      DeathBenefit : {
        type: GraphQLString,
        description: 'The death benefit of the policy',
      },
      ProductName : {
        type: GraphQLString,
        description: 'The name of the product',
      },
      PolicyDescription : {
        type: GraphQLString,
        description: 'The description of the policy',
      }
      */
  }),
});

/**
 * This is the type that will be the root of our mutations,
 * and the entry point into performing writes in our schema.
 */
var mutationType = new GraphQLObjectType({
  name: 'Mutation',
  fields: () => ({
    // Add your own mutations here
  })
});

/**
 * Finally, we construct our schema (whose starting query type is the query
 * type we defined above) and export it.
 */
export var Schema = new GraphQLSchema({
  query: queryType
  // Uncomment the following after adding some mutation fields:
  // mutation: mutationType
});
