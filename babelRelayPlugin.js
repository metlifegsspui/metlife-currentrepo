

var babelRelayPlugin = require('babel-relay-plugin');
var introspectionQuery = require('graphql/utilities').introspectionQuery;
var request = require('sync-request');

var jsonfile = require('jsonfile');

//var graphqlUrl = 'http://localhost:8793/graphql';
//var graphqlUrl = 'http://172.26.60.5:8793/graphql'
/*
var response = request('POST', graphqlUrl, {
	body: introspectionQuery,
	headers: {'Content-Type': 'application/json'}
	});

var schema = JSON.parse(response.body.toString('utf-8'));
*/
//console.log("schema is: "+JSON.stringify(schema));


var schema = require('./mock/schema.json');

module.exports = babelRelayPlugin(schema.data, {
	abortOnError: true
});

//var schema = require('././schema/schema.json');

//module.exports = babelRelayPlugin(schema.data);

//Mock
var express = require('express');

var app  = express();
var PORT = 3000;

app.use(function (req, res, next){
	res.setHeader('Access-Control-Allow-Origin', 'http://localhost:7776');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
	next();
});

app.post('/graphql', (req, res) => {
	res.send({"data":{"node":{"id":"dXNlcjpkWE5sY2pwdWRXeHM=","__typename":"User","_policieswxhi57":{"edges":[{"node":{"id":"0","memberInfo":{"firstName":"Cathy","lastName":"Doe","dateOfBirth":"1950-07-13"},"billingInfo":{"paymentDueAmt":"60.09","billingOption":"Debit"},"beneficiaries":[{"firstName":"Jane","lastName":"Doe","type":"Secondary","beneficiaryId":"12221112"},{"firstName":"Li","lastName":"Ma","type":"Primary","beneficiaryId":"189893"}],"deathBenefit":"1809800.99","status":"Active","accountNumber":"99999999","faceAmt":"250080.90","accountType":"Life","accountLoB":"flexiblePremium","maxYearlyBenefit":null,"deductible":null,"lumpSumBenefit":null,"monthlyBenefit":null,"RefDataItems":[{"RefDataItem":[{"RefId":"\"IWANTTO_OPTIONS\"","lookupid":"ADDBEN","lookupvalue":"Add a Beneficiary"},{"RefId":"\"IWANTTO_OPTIONS\"","lookupid":"FILECLAIM","lookupvalue":"File a Claim"},{"RefId":"\"IWANTTO_OPTIONS\"","lookupid":"UPDADDRESS","lookupvalue":"Update Address"}],"RefId":[{"RefId":"I Want To..."}]}]},"cursor":"cursor1-1"},{"node":{"id":"1","memberInfo":{"firstName":"Cathy","lastName":"Doe","dateOfBirth":"1950-07-13"},"billingInfo":{"paymentDueAmt":"40.82","billingOption":"CC"},"beneficiaries":null,"deathBenefit":null,"status":"Active","accountNumber":"8888888","faceAmt":null,"accountType":"Dental","accountLoB":"omniAdvantage","maxYearlyBenefit":"10000.00","deductible":"80.00","lumpSumBenefit":null,"monthlyBenefit":null,"RefDataItems":[{"RefDataItem":[{"RefId":"\"IWANTTO_OPTIONS\"","lookupid":"ADDBEN","lookupvalue":"Add a Beneficiary"},{"RefId":"\"IWANTTO_OPTIONS\"","lookupid":"FILECLAIM","lookupvalue":"File a Claim"},{"RefId":"\"IWANTTO_OPTIONS\"","lookupid":"UPDADDRESS","lookupvalue":"Update Address"}],"RefId":[{"RefId":"I Want To..."}]}]},"cursor":"cursor1-2"},{"node":{"id":"2","memberInfo":{"firstName":"Cathy","lastName":"Doe","dateOfBirth":"1950-07-13"},"billingInfo":{"paymentDueAmt":"48.56","billingOption":"Direct Deposit"},"beneficiaries":[{"firstName":"Li","lastName":"Ma","type":"Primary","beneficiaryId":"189893"}],"deathBenefit":null,"status":"Active- Perm Pay","accountNumber":"34564547","faceAmt":null,"accountType":"Annuity","accountLoB":"preferencePlusSelect","maxYearlyBenefit":null,"deductible":null,"lumpSumBenefit":"8999.62","monthlyBenefit":"230.00","RefDataItems":[{"RefDataItem":[{"RefId":"\"IWANTTO_OPTIONS\"","lookupid":"ADDBEN","lookupvalue":"Add a Beneficiary"},{"RefId":"\"IWANTTO_OPTIONS\"","lookupid":"FILECLAIM","lookupvalue":"File a Claim"},{"RefId":"\"IWANTTO_OPTIONS\"","lookupid":"UPDADDRESS","lookupvalue":"Update Address"}],"RefId":[{"RefId":"I Want To..."}]}]},"cursor":"cursor1-3"}],"pageInfo":{"hasNextPage":false,"hasPreviousPage":false}}}}});
});

app.options('/graphql', (req, res) => {
	res.send('');
});

var server = app.listen(PORT, function () {
	server.address().address = 'localhost';
	var host = server.address().address;
	var port = server.address().port;

	console.log('GraphQL listening at http://%s:%s', host, port);
});

server.listen();
