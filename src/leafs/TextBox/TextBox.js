/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import React, { PropTypes, Component } from 'react';
import withStyles from '../../decorators/withStyles';

class TextBox extends Component {

  static propTypes = {
    maxLines: PropTypes.number,
    placeHolderText: PropTypes.string,
  };

  static defaultProps = {
    maxLines: 1,
    placeHolderText: "",
  };

  render() {
    return (
      <div className="TextBox">
        {this.props.maxLines > 1 ?
          <textarea {...this.props} className="TextBox-input" ref="input" key="input" rows={this.props.maxLines} /> :
          <input {...this.props} className="TextBox-input" ref="input" key="input" placeholder={this.props.placeHolderText} />}
      </div>
    );
  }

}

export default TextBox;
