import React, { PropTypes, Component } from 'react';
import withStyles from '../../decorators/withStyles';
import Link from '../../leafs/Link';

class Progress extends Component {

  static propTypes = {
    position: PropTypes.number,
  };

  static defaultProps = {
    position: 1
  };

  render() {
    return (
      <div id="Progress">
        <a href="/register" onClick={Link.handleClick} className="StepOne blue">1</a>
        <div className="ProgressLine"></div>
        {this.props.position == 1 ?
          <a href="/register" onClick={Link.handleClick} className="StepTwo">2</a> :
          <a href="/register" onClick={Link.handleClick} className="StepTwo blue">2</a> }
      </div>
    );
  }
}

export default Progress;
