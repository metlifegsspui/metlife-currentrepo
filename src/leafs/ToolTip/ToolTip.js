import React, { PropTypes, Component } from 'react';
import withStyles from '../../decorators/withStyles';

class ToolTip extends Component {

  static propTypes = {
    toolTipText: PropTypes.string
  };

  static defaultProps = {
    toolTipText: ""
  };

  render() {
    return (
      <div className="ToolTip">
        <div className="ToolTipImage"></div>
        <div className="ToolTipBox">
          <label id="ToolTipText">{this.props.toolTipText}</label>
        </div>
      </div>

    );
  }

}

export default ToolTip;
