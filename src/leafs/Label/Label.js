import React, { PropTypes, Component } from 'react';
import withStyles from '../../decorators/withStyles';

class Label extends Component {

  static propTypes = {
    text: PropTypes.string,
    labelType: PropTypes.string
  };

  static defaultProps = {
    labelType: ""
  };

  render() {
    return (
      <label className={this.props.labelType}>
        {this.props.text}
      </label>
    );
  }
}

export default Label;
