/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import React, { PropTypes, Component } from 'react';
import withStyles from '../../decorators/withStyles';

class Heading extends Component {

  static propTypes = {
    headingType: PropTypes.string,
    text: PropTypes.string,
  };

  static defaultProps = {
    headingType: "h1",
  };

  render() {
    return (
      <div className={this.props.headingType}>
        {this.props.text}
      </div>
    );
  }

}

export default Heading;
