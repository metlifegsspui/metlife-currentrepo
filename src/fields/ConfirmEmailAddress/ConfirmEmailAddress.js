import React, { PropTypes, Component } from 'react';
import withStyles from '../../decorators/withStyles';
import Link from '../../leafs/Link';
import TextBox from '../../leafs/TextBox';
import Label from '../../leafs/Label';

class ConfirmEmailAddress extends Component {

  render() {
    return (
      <div id="ConfirmEmailAddress">
        <Label text="Confirm Email Address"/>
        <TextBox id="confirmEmailAddress" />
        <div className="clear"></div>
      </div>
    );
  }
}

export default ConfirmEmailAddress;
