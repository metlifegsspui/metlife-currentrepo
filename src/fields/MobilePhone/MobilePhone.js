import React, { PropTypes, Component } from 'react';
import withStyles from '../../decorators/withStyles';
import Link from '../../leafs/Link';
import TextBox from '../../leafs/TextBox';
import Label from '../../leafs/Label';

class MobilePhone extends Component {

  render() {
    return (
      <div id="MobilePhone">
        <Label text="Mobile Phone"/>
        <TextBox id="mobilePhone" />
        <div className="clear"></div>
      </div>
    );
  }
}

export default MobilePhone;
