import React, { PropTypes, Component } from 'react';
import withStyles from '../../decorators/withStyles';
import Link from '../../leafs/Link';
import TextBox from '../../leafs/TextBox';
import Label from '../../leafs/Label';

class DateOfBirth extends Component {

  static contextTypes = {
    onSetTitle: PropTypes.func.isRequired,
  };

  render() {
    const dateOfBirth = 'DD / MM/ YYYY';
    this.context.onSetTitle(dateOfBirth);

    return (
      <div id="DateOfBirth">
        <Label text="Date of Birth"/>
        <TextBox id="dateOfBirth" placeHolderText={dateOfBirth} />
        <div className="clear"></div>
      </div>
    );
  }
}

export default DateOfBirth;
