import React, { PropTypes, Component } from 'react';
import withStyles from '../../decorators/withStyles';
import Link from '../../leafs/Link';
import TextBox from '../../leafs/TextBox';
import Label from '../../leafs/Label';

class LastName extends Component {

  render() {
    return (
      <div id="LastName">
        <Label text="Last Name"/>
        <TextBox id="lastName" />
        <div className="clear"></div>
      </div>
    );
  }
}

export default LastName;
