import React, { PropTypes, Component } from 'react';
import withStyles from '../../decorators/withStyles';
import Link from '../../leafs/Link';
import TextBox from '../../leafs/TextBox';
import Label from '../../leafs/Label';

class ZipCode extends Component {

  render() {
    return (
      <div id="ZipCode">
        <Label text="Zip Code"/>
        <TextBox id="zipCode" />
        <div className="clear"></div>
      </div>
    );
  }
}

export default ZipCode;
