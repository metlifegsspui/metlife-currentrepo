import React, { PropTypes, Component } from 'react';
import withStyles from '../../decorators/withStyles';
import Link from '../../leafs/Link';
import TextBox from '../../leafs/TextBox';
import Label from '../../leafs/Label';

class SocialSecurityNumber extends Component {

  render() {
    return (
      <div id="SocialSecurityNumber">
        <Label text="Social Security Number"/>
        <TextBox id="socialSecurityNumber" />
        <div className="clear"></div>
      </div>
    );
  }
}

export default SocialSecurityNumber;
