import React, { PropTypes, Component } from 'react';
import withStyles from '../../decorators/withStyles';
import Link from '../../leafs/Link';
import TextBox from '../../leafs/TextBox';
import Label from '../../leafs/Label';
import ToolTip from '../../leafs/ToolTip';

class FirstName extends Component {

  render() {
    return (
      <div id="FirstName">
        <Label text="First Name"/>
        <TextBox id="firstName" />

        <div className="clear"></div>
        <ToolTip toolTipText="Please enter your first name as it appears on your Metlfe Policy"/>
      </div>
    );
  }
}

export default FirstName;
