import React, { PropTypes, Component } from 'react';
import withStyles from '../../decorators/withStyles';
import Link from '../../leafs/Link';
import TextBox from '../../leafs/TextBox';
import Label from '../../leafs/Label';

class EmailAddress extends Component {

  render() {
    return (
      <div id="EmailAddress">
        <Label text="Email Address"/>
        <TextBox id="emailAddress" />
        <div className="clear" />
      </div>
    );
  }
}

export default EmailAddress;
