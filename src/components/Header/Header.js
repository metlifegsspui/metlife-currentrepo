/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import React, { Component } from 'react';
import withStyles from '../../decorators/withStyles';
import Link from '../../leafs/Link';
import Navigation from '../Navigation';

class Header extends Component {
  render() {
    console.log("Header!");
    return (
      <header>
        HEADER
        <Navigation />
      </header>
    );
  }

}

export default Header;
