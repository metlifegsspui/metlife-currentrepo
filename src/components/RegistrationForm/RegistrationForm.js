import React, { PropTypes, Component } from 'react';
import withStyles from '../../decorators/withStyles';
import Link from '../../leafs/Link';
import TextBox from '../../leafs/TextBox';
import Label from '../../leafs/Label';
import Heading from '../../leafs/Heading';
import FirstName from '../../fields/FirstName';
import LastName from '../../fields/LastName';
import DateOfBirth from '../../fields/DateOfBirth';
import SocialSecurityNumber from '../../fields/SocialSecurityNumber';
import ZipCode from '../../fields/ZipCode';
import EmailAddress from '../../fields/EmailAddress';
import ConfirmEmailAddress from '../../fields/ConfirmEmailAddress';
import MobilePhone from '../../fields/MobilePhone';


class RegistrationForm extends Component {

  static contextTypes = {
    onSetTitle: PropTypes.func.isRequired,
  };

  render() {

    return (
      <form id="RegisterForm">
        <section className="RegistrationInformation">
          <Heading text="Registration is quick and easy." headingType="h4"/>
          <a className="Learn-link" href="/learn" onClick={Link.handleClick}>Learn more about who can register</a>
          <Label text="Enter your identification and contact information below."/>
          <Label text="All fields are required unless noted" labelType="information"/>
        </section>

        <section className="RegistrationFields">
          <FirstName />
          <LastName />
          <DateOfBirth />
          <SocialSecurityNumber />
          <ZipCode />
          <EmailAddress />
          <ConfirmEmailAddress />
          <MobilePhone />
        </section>
      </form>
    );
  }
}

export default RegistrationForm;
