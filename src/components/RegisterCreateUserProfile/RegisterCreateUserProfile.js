import React, { PropTypes, Component } from 'react';
import withStyles from '../../decorators/withStyles';
import Link from '../../leafs/Link';
import Heading from '../../leafs/Heading';
import RegistrationForm from '../RegistrationForm';
import Progress from '../../leafs/Progress';

class RegisterCreateUserProfile extends Component {

  static contextTypes = {
    onSetTitle: PropTypes.func.isRequired,
  };

  render() {
    const title = 'Register to view your online accounts';
    const nextButtonText = 'Next';
    const cancelButtonText = 'Cancel';
    this.context.onSetTitle(title);
    this.context.onSetTitle(nextButtonText);

    return (
      <div className="RegisterCreateUserProfile">

        <section className="OutsidePanel">
          <Heading text={title} headingType="h1"/>
          <Progress position={1} />
        </section>
        <RegistrationForm />
        <br />
        <a href="/register" onClick={Link.handleClick} className="ClearButton">{cancelButtonText}</a>
        <a href="/register/2" onClick={Link.handleClick} className="GenericButton">{nextButtonText}</a>

      </div>
    );
  }
}

export default RegisterCreateUserProfile;
