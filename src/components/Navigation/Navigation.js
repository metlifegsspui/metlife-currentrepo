/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import React, { PropTypes, Component } from 'react';
import classNames from 'classnames';
import withStyles from '../../decorators/withStyles';
import Link from '../../leafs/Link';

class Navigation extends Component {

  static propTypes = {
    className: PropTypes.string,
  };

  render() {
    return (
      <nav id="Navigation" className={classNames(this.props.className)} role="navigation">
        <a className="Navigation-link" href="/login" onClick={Link.handleClick}>Already registered?</a>
        <a className="Navigation-link" href="/login" onClick={Link.handleClick}>LOGIN</a>
      </nav>
    );
  }

}

export default Navigation;
